package me.pqpo.librarylog4a.utils;

import ohos.hiviewdfx.HiLog;
import ohos.hiviewdfx.HiLogLabel;

/**
 *  封 HiLog
 *
 * Copyright (C) 2021 Huawei Device Co., Ltd.
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 *     http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
public abstract class Log {
    private static final String TAG = "aaa";
    private static final int DOMAIN = 0x01;
    private static final HiLogLabel LOG_LABLE = new HiLogLabel(HiLog.LOG_APP, DOMAIN, TAG);

    public static void i(String message) {
        HiLog.info(LOG_LABLE, "%{public}s", message);
    }

    public static void i(String tag, String message) {
        final HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.info(logLabel, "%{public}s", message);
    }


    public static void e(String message) {
        HiLog.error(LOG_LABLE, "%{public}s", message);
    }

    public static void e(String tag, String message) {
        final HiLogLabel logLabel = new HiLogLabel(HiLog.LOG_APP, DOMAIN, tag);
        HiLog.error(logLabel, "%{public}s", message);
    }

    public static void d(String tag, String message) {
        HiLog.debug(LOG_LABLE, "%{public}s", message);
    }

    public static boolean isDebug() {
        return HiLog.isLoggable(DOMAIN, TAG, HiLog.DEBUG);
    }

    public static void d(String s) {
    }
}
