package me.pqpo.librarylog4a.logger;

import me.pqpo.librarylog4a.utils.Log;

/**
 * Created by pqpo on 2018/2/27.
 */

public class HarmonyLogger implements Logger {

    @Override
    public void println(int priority, String tag, String msg) {
        Log.d(tag, msg);
    }

    @Override
    public void flush() {

    }

    @Override
    public void release() {

    }

}
