package me.pqpo.librarylog4a.appender;

import java.util.ArrayList;
import java.util.List;

import me.pqpo.librarylog4a.Level;
import me.pqpo.librarylog4a.interceptor.Interceptor;
import me.pqpo.librarylog4a.utils.Log;

/**
 * Created by pqpo on 2017/11/16.
 */
public class HarmonyAppender extends AbsAppender {

    protected HarmonyAppender(Builder builder) {
        setLevel(builder.level);
        addInterceptor(builder.interceptors);
    }

    @Override
    protected void doAppend(int logLevel, String tag, String msg) {
        Log.d(tag, msg);
    }

    public static class Builder {

        private int level = Level.VERBOSE;
        private List<Interceptor> interceptors;

        public Builder setLevel(int level) {
            this.level = level;
            return this;
        }

        public Builder addInterceptor(Interceptor interceptor) {
            if (interceptors == null) {
                interceptors = new ArrayList<>();
            }
            interceptors.add(interceptor);
            return this;
        }

        public HarmonyAppender create() {
            return new HarmonyAppender(this);
        }

    }

}
