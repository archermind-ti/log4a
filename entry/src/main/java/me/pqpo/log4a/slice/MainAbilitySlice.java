package me.pqpo.log4a.slice;


import me.pqpo.librarylog4a.Log4a;
import me.pqpo.librarylog4a.appender.AbsAppender;
import me.pqpo.librarylog4a.appender.HarmonyAppender;
import me.pqpo.librarylog4a.appender.Appender;
import me.pqpo.librarylog4a.appender.FileAppender;
import me.pqpo.librarylog4a.formatter.Formatter;
import me.pqpo.librarylog4a.logger.AppenderLogger;
import me.pqpo.librarylog4a.logger.Logger;
import me.pqpo.log4a.FileUtils;
import me.pqpo.log4a.LogInit;
import me.pqpo.log4a.ResourceTable;
import me.pqpo.log4a.append.BufferFileAppender;
import me.pqpo.log4a.append.NoBufferFileAppender;
import me.pqpo.log4a.append.NoBufferInThreadFileAppender;
import ohos.aafwk.ability.AbilitySlice;
import ohos.aafwk.content.Intent;
import ohos.agp.components.Button;
import ohos.agp.components.Component;
import ohos.agp.components.Text;
import ohos.agp.components.TextField;
import ohos.agp.window.dialog.ToastDialog;

import java.io.File;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import ohos.eventhandler.EventHandler;
import ohos.eventhandler.EventRunner;

import static me.pqpo.log4a.LogInit.BUFFER_SIZE;

public class MainAbilitySlice extends AbilitySlice {

    private static final String TAG = "MainAbilitySlice";

    TextField etContent;
    TextField etThread;
    Button btnWrite;
    Button btnTest;
    Text tvTest;
    TextField etTimes;

    ToastDialog toastDialog;

    boolean testing = false;


    @Override
    public void onStart(Intent intent) {
        super.onStart(intent);
        super.setUIContent(ResourceTable.Layout_ability_main);

        toastDialog = new ToastDialog(getContext());

        // log threads
        etThread = (TextField) findComponentById(ResourceTable.Id_et_thread);
        // log content
        etContent = (TextField) findComponentById(ResourceTable.Id_et_content);
        // button write log
        btnWrite = (Button) findComponentById(ResourceTable.Id_btn_write);
        // button text
        btnTest = (Button) findComponentById(ResourceTable.Id_btn_test);
        // 最下面的 text, 点击 test
        tvTest = (Text) findComponentById(ResourceTable.Id_tv_test);
        // log times for test
        etTimes = (TextField) findComponentById(ResourceTable.Id_et_times);

        // 点击 write log
        btnWrite.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (testing) {
                    toastDialog.setText("testing")
                            .setDuration(0)
                            .show();
                    return;
                }
                int threads = Integer.valueOf(etThread.getText().toString());
                if (threads > 500) {
                    toastDialog.setText("Do not exceed 500 threads")
                            .setDuration(0)
                            .show();
                    return;
                }
                final String str = etContent.getText().toString();
                for (int i = 0; i < threads; i++) {
                    new Thread() {
                        @Override
                        public void run() {
                            super.run();
                            Log4a.i(TAG, str);
                        }
                    }.start();
                }
                tvTest.setText("done!\nlog file path:" + getLogPath());
            }
        });

        // 点击TEST
        btnTest.setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                if (!testing) {
                    tvTest.setText("start testing\n");
                    testing = true;
                    int times = Integer.valueOf(etTimes.getText().toString());
                    performanceTest(times);
                    testing = false;
                } else {
                    toastDialog.setText("testing")
                            .setDuration(0)
                            .show();
                }
            }
        });

        // 点击 按钮 change log path
        findComponentById(ResourceTable.Id_btn_change_log).setClickedListener(new Component.ClickedListener() {
            @Override
            public void onClick(Component component) {
                changeLogPath();
            }
        });


    }

    private void performanceTest(int times) {
        tvTest.append(String.format(Locale.getDefault(),
                "## prints %d logs:\n", times));
        Log4a.release();
        AppenderLogger logger = new AppenderLogger.Builder().create();
        Log4a.setLogger(logger);

        logger.addAppender(createLog4aFileAppender());
        doPerformanceTest("log4a", times);
        Log4a.release();

        Log4a.setLogger(logger);
        logger.addAppender(createHiLogAppender());
        doPerformanceTest("HiLog log", times);
        Log4a.release();

        Log4a.setLogger(logger);
        List<String> buffer = new ArrayList<>(times);
        logger.addAppender(createMemAppender(buffer));
        doPerformanceTest("array list log", times);
        buffer.clear();
        Log4a.release();

        Log4a.setLogger(logger);
        logger.addAppender(createNoBufferFileAppender());
        doPerformanceTest("file log(no buffer)", times);
        Log4a.release();

        Log4a.setLogger(logger);
        logger.addAppender(createWithBufferFileAppender());
        doPerformanceTest("file log(with buffer)", times);
        Log4a.release();

        Log4a.setLogger(logger);
        logger.addAppender(createNoBufferInThreadFileAppender());
        doPerformanceTest("file log(no buffer in thread)", times);

        EventHandler eventHandler = new EventHandler(EventRunner.getMainEventRunner());
        eventHandler.postTask(new Runnable() {
            @Override
            public void run() {
                Log4a.release();
            }
        }, 1000);

        LogInit.init(this);
        tvTest.append("## end");
    }

    private Appender createNoBufferFileAppender() {
        File logFile = new File(FileUtils.getLogDir(this), "logNoBufferFileTest.txt");
        logFile.delete();
        return new NoBufferFileAppender(logFile);
    }

    private Appender createWithBufferFileAppender() {
        File logFile = new File(FileUtils.getLogDir(this), "logBufferFileTest.txt");
        logFile.delete();
        return new BufferFileAppender(logFile, BUFFER_SIZE);
    }

    private Appender createNoBufferInThreadFileAppender() {
        File logFile = new File(FileUtils.getLogDir(this), "logNoBufferInThreadFileTest.txt");
        logFile.delete();
        return new NoBufferInThreadFileAppender(logFile);
    }

    private Appender createMemAppender(final List<String> buffer) {
        return new AbsAppender() {
            @Override
            protected void doAppend(int logLevel, String tag, String msg) {
                buffer.add(msg);
            }
        };
    }

    private Appender createLog4aFileAppender() {
        File log = FileUtils.getLogDir(this);
        File cacheFile = new File(log, "test.logCache");
        File logFile = new File(log, "log4aTest.txt");
        cacheFile.delete();
        logFile.delete();
        FileAppender.Builder fileBuild = new FileAppender.Builder(this)
                .setLogFilePath(logFile.getAbsolutePath())
                .setBufferSize(BUFFER_SIZE)
                .setFormatter(new Formatter() {
                    @Override
                    public String format(int logLevel, String tag, String msg) {
                        return msg;
                    }
                })
                .setBufferFilePath(cacheFile.getAbsolutePath());

        return fileBuild.create();
    }

    private Appender createHiLogAppender() {
        return new HarmonyAppender.Builder().create();
    }

    private void doPerformanceTest(String testName, int times) {
        long currentTimeMillis = System.currentTimeMillis();
        for (int i = 0; i < times; i++) {
            Log4a.i(TAG, "log-" + i);
        }
        tvTest.append(String.format(Locale.getDefault(),
                "* %s spend: %d ms\n",
                testName,
                System.currentTimeMillis() - currentTimeMillis));
    }

    public String getLogPath() {
        String logPath = "";
        Logger logger = Log4a.getLogger();
        if (logger instanceof AppenderLogger) {
            List<Appender> appenderList = ((AppenderLogger) logger).getAppenderList();
            for (Appender appender : appenderList) {
                if (appender instanceof FileAppender) {
                    FileAppender fileAppender = (FileAppender) appender;
                    logPath = fileAppender.getLogPath();
                    break;
                }
            }
        }
        return logPath;
    }

    public void changeLogPath() {
        Logger logger = Log4a.getLogger();
        if (logger instanceof AppenderLogger) {
            List<Appender> appenderList = ((AppenderLogger) logger).getAppenderList();
            for (Appender appender : appenderList) {
                if (appender instanceof FileAppender) {
                    FileAppender fileAppender = (FileAppender) appender;
                    File log = FileUtils.getLogDir(getContext());
                    String time = new SimpleDateFormat("yyyy_MM_dd", Locale.getDefault()).format(new Date());
                    String logPath = new File(log, time + "-" + System.currentTimeMillis() + ".txt").getAbsolutePath();
                    fileAppender.changeLogPath(logPath);
                    break;
                }
            }

        }
    }

    @Override
    public void onActive() {
        super.onActive();
    }

    @Override
    public void onForeground(Intent intent) {
        super.onForeground(intent);
    }

    @Override
    public void onBackPressed() {
        super.onBackPressed();
        Log4a.flush();
    }

    @Override
    public void onBackground(){
        super.onBackground();
        Log4a.flush();
    }
}
