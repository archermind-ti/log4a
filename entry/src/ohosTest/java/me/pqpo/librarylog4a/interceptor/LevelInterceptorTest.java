package me.pqpo.librarylog4a.interceptor;

import me.pqpo.librarylog4a.LogData;
import org.junit.Test;
import static org.junit.Assert.*;

public class LevelInterceptorTest {

    @Test
    public void intercept() {
        LevelInterceptor levelInterceptor = new LevelInterceptor();
        LogData logData = LogData.obtain(4, "MainAbilitySlice", "");
        assertTrue(levelInterceptor.intercept(logData));
    }
}