package me.pqpo.librarylog4a;

import me.pqpo.librarylog4a.LogBuffer;
import org.junit.Test;
import static org.junit.Assert.*;

public class LogBufferTest {

    private static final String bufferPath = "/storage/emulated/0/Android/data/me.pqpo.log4a/files/logs/test.logCache";
    private static final String logPath = "/storage/emulated/0/Android/data/me.pqpo.log4a/files/logs/log4aTest.txt";
    private static final int bufferSize = 409600;

    private final LogBuffer logBuffer = new LogBuffer(bufferPath,409600,logPath,false);

    @Test
    public void isCompress() {
        assertFalse(logBuffer.isCompress());
    }

    @Test
    public void getLogPath() {
        assertEquals(logPath, logBuffer.getLogPath());
    }

    @Test
    public void getBufferPath() {
        assertEquals(bufferPath, logBuffer.getBufferPath());
    }

    @Test
    public void getBufferSize() {
        assertEquals(bufferSize, logBuffer.getBufferSize());
    }

}