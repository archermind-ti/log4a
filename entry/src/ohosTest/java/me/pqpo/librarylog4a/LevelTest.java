package me.pqpo.librarylog4a;

import me.pqpo.librarylog4a.Level;
import org.junit.Test;
import static org.junit.Assert.*;

public class LevelTest {

    @Test
    public void getLevelName() {
        assertEquals("INFO", Level.getLevelName(4));
    }

    @Test
    public void getShortLevelName() {
        assertEquals("I", Level.getShortLevelName(4));
    }
}