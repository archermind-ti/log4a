package me.pqpo.librarylog4a.formatter;

import me.pqpo.librarylog4a.formatter.DateFileFormatter;
import org.junit.Test;


import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;

import static org.junit.Assert.*;

public class DateFileFormatterTest {
    private final StringBuffer text = new StringBuffer();
    private final DateFileFormatter dateFileFormatter = new DateFileFormatter();
    @Test
    public void format() {
        String result;
        SimpleDateFormat formatter = new SimpleDateFormat("yyyy:MM:dd HH:mm:ss", Locale.getDefault());
        Date curDate =  new Date(System.currentTimeMillis());
        text.append(formatter.format(curDate));
        result = text.append(" ILog4a-MainAbilitySlice").append('\n').toString();
        assertEquals(dateFileFormatter.format(4,"Log4a-MainAbilitySlice",""),result);
    }
}